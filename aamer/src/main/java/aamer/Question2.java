package aamer;

public class Question2 {
	
	public String CheckCharacter(char ch) {
		
		if(Character.isDigit(ch)) {
			return "Digit";
		}
		else if(Character.isUpperCase(ch)) {
			return "Uppercase";
		}
		else if(Character.isLowerCase(ch)) {
			return "Lowercase";
		}
		else  {
			return "Others";
			
		}
		
	}

}
