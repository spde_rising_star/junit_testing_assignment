package aamer;

//Write unit tests for Implement the main and child threads by using runnable interface. Implement the same

//functionality using lambda expressions

public class Question37 {

	static boolean flag = false;

	public static Runnable getRunnable() {
		Runnable r = () -> {
			for (int i = 0; i < 10; i++) {
				System.out.println("child thread");
			}
			flag = true;
		};
		return r;
	}

	public static void main(String[] args) {

		Thread t = new Thread(getRunnable());

		t.start();
		for (int i = 0; i < 10; i++) {
			System.out.println("main thread");
		}

	}
}

