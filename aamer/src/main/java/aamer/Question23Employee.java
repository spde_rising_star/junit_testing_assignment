package aamer;

import java.util.ArrayList;
import java.util.Collections;

public class Question23Employee {
    private int id;
    private String name;
    private String department;



   public String getName() {
        return name;
    }



   public void setName(String name) {
        this.name = name;
    }



   public Question23Employee(int id, String name, String department) {
        super();
        this.id = id;
        this.name = name;
        this.department = department;
    }



   public static ArrayList<Question23Employee > sortEmployee(ArrayList<Question23Employee > empList) {
        Collections.sort(empList, (a, b) -> a.getName().compareTo(b.getName()));
        ;
        return empList;
    }



   @Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + name + ", department=" + department + "]";
    }



   @Override
    public boolean equals(Object obj) {



       return ((Integer) ((Question23Employee ) (obj)).id).equals(this.id);
    }



   public static void main(String[] args) {
        ArrayList<Question23Employee > empList = new ArrayList<>();



       empList.add(new Question23Employee (1, "waseem", "Java"));
        empList.add(new Question23Employee (2, "younus", "Java"));
        empList.add(new Question23Employee (3, "Aamer", "Java"));
    
        System.out.println(sortEmployee(empList));
    }



}