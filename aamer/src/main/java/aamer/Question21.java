package aamer;

import java.util.function.BiFunction;

//Write unit tests for prepare addition of two numbers method and implement the same using lambda 
//expression

interface addition {
	public int add(int a, int b);
}

public class Question21 {

	public static BiFunction<Integer, Integer, Integer> add = (a, b) -> a + b;

	public static void main(String[] args) {

		System.out.println(add.apply(1, 2));

	}
}

