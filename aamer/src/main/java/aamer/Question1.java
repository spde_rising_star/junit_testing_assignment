package aamer;

public class Question1 {

	static int staticval=0;
	int nonstaticval=0;
	
	public void increment() {
		staticval++;
		nonstaticval++;
	}
	
	public int printstat() {
	   return staticval;
		
	}
	
	public int printnonstat() {
		return nonstaticval;
	}
	
	public static void main(String[] as) {
		Question1 obj1= new Question1();
		obj1.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
		
		Question1 obj2= new Question1();
		obj2.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
		
		Question1 obj3= new Question1();
		obj3.increment();
		System.out.println(obj1.printstat());
		System.out.println(obj1.printnonstat());
	}
}

