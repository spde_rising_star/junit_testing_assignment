package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class SELECT {
	private static final String Select_Query = "SELECT EmpId,EmpName,EmpEmail,EmpGender,Empsalary FROM EMPUNIT";

	public static void main(String[] as) {
		Scanner sc=new Scanner(System.in);
		
		// load the jdbc driver
		
		try {
			Class.forName("com.mysql.cj.jdc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create the connection
		
	try(Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/junit_emp_db", "root", "root")){
		PreparedStatement ps= con.prepareStatement(Select_Query);
		// Create resultSet object
		ResultSet rs=ps.executeQuery();
		
		//fetch the data by row
		while(rs.next()) {
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getFloat(5));
		}
	} catch (SQLException se) {
		System.out.println(se.getMessage());
		se.printStackTrace();
	}
	catch(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
	}
		
}


