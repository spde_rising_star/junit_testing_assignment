package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class DELETE {
	
	private static final String DELETE_QUERY ="DELETE FROM EMPUNIT WHERE EMPID=?";

	public static void main(String[] as) {
		Scanner sc=new Scanner(System.in);
		
		// load the jdbc driver
		
		try {
			Class.forName("com.mysql.cj.jdc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create the connection
		
	try(Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/junit_emp_db", "root", "root")){
		PreparedStatement ps= con.prepareStatement(DELETE_QUERY);
		System.out.println("Enter EmpId Which we have to delete:");
		int eid= sc.nextInt();
		
		//set the values
		ps.setInt(1,eid);
		
		
		//execute the query
		int count=ps.executeUpdate();
		if(count==0) {
			System.out.println("Record is  not deleted");
		}
		else {
			System.out.println("Record is  deleted");

		}
	} catch (SQLException se) {
		System.out.println(se.getMessage());
		se.printStackTrace();
	}
	catch(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
	}
		
}
