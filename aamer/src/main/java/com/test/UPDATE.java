package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class UPDATE {

	private static final String Update_Query = "UPDATE  EMPUNIT SET EMPNAME=? WHERE EMPID=?";
	public static void main(String[] as) {
		Scanner sc=new Scanner(System.in);
		
		// load the jdbc driver
		
		try {
			Class.forName("com.mysql.cj.jdc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create the connection
		
	try(Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/junit_emp_db", "root", "root")){
		PreparedStatement ps= con.prepareStatement(Update_Query);
		System.out.println("Enter EmpId:");
		int eid= sc.nextInt();
		System.out.println("Enter updation EmpName:");
		String ename=sc.next();
		
		//set the values
		ps.setString(1,ename);
		ps.setInt(2,eid);
		
		//execute the query
		int count=ps.executeUpdate();
		if(count==0) {
			System.out.println(" Record is  not updated");
		}
		else {
			System.out.println("Record is updated");

		}
	} catch (SQLException se) {
		System.out.println(se.getMessage());
		se.printStackTrace();
	}
	catch(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
	}
		
}
