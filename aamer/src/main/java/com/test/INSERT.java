package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class INSERT {
	
	private static final String Insert_Query = "INSERT INTO EMPUNIT(EmpId,EmpName,EmpEmail,EmpGender,Empsalary) VALUES (?,?,?,?,?)";

	public static void main(String[] as) {
		Scanner sc=new Scanner(System.in);
		
		// load the jdbc driver
		
		try {
			Class.forName("com.mysql.cj.jdc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create the connection
		
	try(Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/junit_emp_db", "root", "root")){
		PreparedStatement ps= con.prepareStatement(Insert_Query);
		System.out.println("Enter EmpId:");
		int eid= sc.nextInt();
		System.out.println("Enter EmpName:");
		String ename=sc.next();
		System.out.println("Enter EmpEmail:");
		String eemail=sc.next();
		System.out.println("Enter EmpGender:");
		String egender=sc.next();
		System.out.println("Enter EmpSalary:");
		int esal=sc.nextInt();
		//set the values
		ps.setInt(1,eid);
		ps.setString(2,ename);
		ps.setString(3,eemail);
		ps.setString(4, egender);
		ps.setFloat(5,esal);
		
		//execute the query
		int count=ps.executeUpdate();
		if(count==0) {
			System.out.println("Record is  not registered");
		}
		else {
			System.out.println("Record is  registered");

		}
	} catch (SQLException se) {
		System.out.println(se.getMessage());
		se.printStackTrace();
	}
	catch(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
	}
		
}

