package aamer;

import static org.junit.Assert.*;

import java.io.FileInputStream;

import org.junit.Test;

public class MultipleFileTest {

	@Test
	public void test() 
	{
		try
		{ 
	    FileInputStream f1=new FileInputStream("C:\\Users\\psale\\Desktop\\FileOutput.txt"); 
	    FileInputStream f2=new FileInputStream("C:\\Users\\psale\\Desktop\\FileOutput1.txt"); 
		int i=0; 
		while((i=f1.read())!=-1)
		{ 
			System.out.print((char)i); 
	    }
		f1.close(); 
		int i1=0; 
		while((i1=f2.read())!=-1)
		{ 
			System.out.print((char)i1); 
	    }
		assertEquals(i, i1);
		f1.close();
	    }catch(Exception e)
		{
	    	System.out.println(e);
	    } 
	}

}


