package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class Diff2DatesTest {

	@Test
	public void test() {
		
		String start_date= "07-09-2000 01:10:20";
		String end_date= "13-10-2022 01:10:20";
		Diff2Dates obj=new Diff2Dates();
		assertEquals("22 years, 41 days, 0 hours, 0 minutes, 0 seconds",Diff2Dates.findDifference(start_date, end_date));
		

	}

}
