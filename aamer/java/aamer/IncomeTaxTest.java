package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class IncomeTaxTest {

	IncomeTax obj=new IncomeTax();
	@Test
	public void salaryLessThan50000() 
	{
		assertEquals(0, obj.taxPaidByEmployee(40000));
	}

	@Test
	public void salaryLessThan60000() 
	{
		assertEquals(200, obj.taxPaidByEmployee(52000));
	}
	
	@Test
	public void salaryLessThan150000() 
	{
		assertEquals(22000, obj.taxPaidByEmployee(120000));
	}
	
	@Test
	public void salaryOther() 
	{
		assertEquals(64000, obj.taxPaidByEmployee(300000));
	}
}
