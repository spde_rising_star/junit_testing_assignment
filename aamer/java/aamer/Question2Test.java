package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class Question2Test {

	Question2 obj1=new Question2();
	@Test
	public void testDigit() {
		
		assertEquals("Digit",obj1.CheckCharacter('2'));
		
	}
	
	@Test
	public void testUpperCase() {
		
		assertEquals("Uppercase",obj1.CheckCharacter('A'));
		
	}
	
	@Test
	public void testLowerCase() {
		
		assertEquals("Lowercase",obj1.CheckCharacter('a'));
		
	}
	

}
