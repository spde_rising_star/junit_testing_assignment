package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class DuplicateCharTest {
	@Test
	public void test() 
	{
		DuplicateChar d=new DuplicateChar();
		assertArrayEquals(new char[] {'i','n'}, d.duplicate1());
	}

}
