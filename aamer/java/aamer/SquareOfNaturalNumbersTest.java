package aamer;



import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;



class SquareOfNaturalNumbersTest {



   SquareOfNaturalNumbers s=new SquareOfNaturalNumbers();
    @Test
    void test() throws Exception
    {
        List<Integer> input= Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        List<Integer> expected= Arrays.asList(1, 4, 9, 16, 25, 36, 49, 64, 81, 100);
        assertArrayEquals(expected.toArray(),s.squares(input).toArray());
    }



}