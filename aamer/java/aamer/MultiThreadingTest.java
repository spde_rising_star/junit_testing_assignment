package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class MultiThreadingTest {
	MultiThreading a1=new MultiThreading();
	MultiThreading a2=new MultiThreading();
	MultiThreading a3=new MultiThreading();
	MultiThreading a4=new MultiThreading();
	MultiThreading a5=new MultiThreading();
	
	
@Test
public void multithread() 
{
	
	a1.setPriority(10);
	a2.setPriority(9);
	a3.setPriority(5);
	a4.setPriority(7);
	a5.setPriority(6);
	a1.setName("1st Thread");
	a1.setName("2nd Thread");
	a1.setName("3rd Thread");
	a1.setName("4th Thread");
	a1.setName("5th Thread");
	a1.start();
	assertTrue(a1.isAlive());
	a2.start();
	System.out.println(Thread.currentThread());
	assertTrue(a2.isAlive());
	a3.start();
	assertTrue(a3.isAlive());
	a4.start();
	assertTrue(a4.isAlive());
	a5.start();
	assertTrue(a5.isAlive());

}


}