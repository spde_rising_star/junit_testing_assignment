package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class DaemonThreadTest {

	@Test
	public void daemonThread() 
	{
		DaemonThread d=new DaemonThread();
		d.setDaemon(true);
		d.start();
		assertEquals(true, d.isDaemon());
	}
	
	@Test
	public void notDaemonThread() 
	{
		DaemonThread d=new DaemonThread();
		d.start();
		assertNotEquals(true, d.isDaemon());
	}
	
	@Test
	public void DaemonThread() 
	{
		DaemonThread d=new DaemonThread();
		DaemonThread d1=new DaemonThread();
		d.start();
		d1.setDaemon(true);
		d1.start();
		assertEquals(true, d1.isDaemon());
		assertEquals(false, d.isDaemon());
	}

}
	


