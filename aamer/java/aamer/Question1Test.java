package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class Question1Test {

	@Test
	public void test1() {
    Question1 obj1=new Question1();
    obj1.increment();
    assertEquals(1,obj1.nonstaticval);

	}
	
	@Test
	public void test2() {
	Question1 obj2=new Question1();
    obj2.increment();
    assertNotEquals(2,obj2.nonstaticval);

	}

}
