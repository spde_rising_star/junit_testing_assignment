package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyLambdaTest {

	 @Test
	   public void lambdaTest() 
	   {
		   MyLambda l=new MyLambda();
		   assertEquals(30, l.add());
	   }
	   @Test
	   public void normalAdd()
	   {
		   MyLambda l=new MyLambda();
		   assertEquals(40,l.addNormal(20, 20));
		   
	   }
	}