package aamer;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayListEvenNoTest {
	ArrayListEvenNo a=new ArrayListEvenNo();
	@Test
	public void evenNumbersAdd()
	{
		a.saveEvenNumbers(10);
		assertNotEquals(a.printEvenNumbers(), a.saveEvenNumbers());
	}
	
	@Test
	public void searchEvenNumber()
	{
		a.saveEvenNumbers(10);
		assertEquals(8, a.printEvenNumber(8));
	}

}
